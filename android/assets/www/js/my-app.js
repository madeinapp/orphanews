// Initialize your app
var debug = true;
var dynamicPageIndex = 0;
var sezione;
var home = true;
var ruotaFeed;
var menuAperto = false;
var apiLoaded = false;
var preferiti = 0;
var dettaglio = false;
var mySwiperH;
var mySwiperV;
var preferite = "";


vettAnchors = Array();
vettTitlesAnchors= Array();
vettTitles= Array();
vettPages= Array();
var myPhotoBrowserPage;

var myApp = new Framework7({
    //swipeLeft: false,
    swipeBackPage:false,
    sortable: true,
    //pushState: true,
    cache: true,
    swipePanelOnlyClose: true,
    panelsCloseByOutside: true
    //cacheDuration: 1000 * 60 * 10,  //10 minuti
    //imagesLazyLoadSequential: true,
    //template7Pages: true,
    //precompileTemplates: true
});

// Export selectors engine
var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true,
    domCache: true,
   // dynamicPageUrl:
});


document.addEventListener("deviceready", function () {
    setTimeout(function (){
        $('body').css('touch-action', 'auto');
        if (! ControllaSeEsiste("preferite")){
            creaOggetto("preferite","", 9999)
        }
        if (! ControllaSeEsiste("lista_preferiti")){
            creaOggetto("lista_preferiti","", 9999)
        }
        if (! ControllaSeEsiste("preferiti")){
            creaOggetto("preferiti",0, 9999)
        }
        //StatusBar.overlaysWebView(false);
        $('#mieNews .miaList ul').html(leggiOggetto("preferite"));
        $('.badge').html(leggiOggetto("preferiti"));
        if (navigator.platform.toLowerCase().indexOf("simulator") == -1){
          initPushwoosh();
        }
        //alert(leggiOggetto("preferite"));0
        //alert(leggiOggetto("preferiti"));
        //alert(leggiOggetto("lista_preferiti"));
        
    },1000);

    $(document).on('click', 'a[target="_blank"]', function (e) {
        e.preventDefault();
        var url = this.href; 
        var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no');                  
    });
    

},false);

$('#skip').click(function () {
    getContents('list','','index');
    $('.sponsor').fadeOut();
    //initPushwoosh();
   
});

$('.pulsMenu').click(function () {
    var pagina = $(this).attr('href');
    mainView.router.load({ pageName: pagina });
    $(this).addClass('active').siblings().removeClass('active');
});

$$('.open-left-panel').on('click', function (e) {
    myApp.openPanel('left');
});
    

function apriTendina(obj) {
    $(obj).nextUntil('.head').slideToggle('slow');
}


$$(document).on('pageAfterAnimation', function (e) {
    var page = e.detail.page;
    getContents('list','', page.name);

});


//function apriUrl(url) {
//        if (checkConnection()) {
//            var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');
//            //top.finestra = window.open(url, '_blank', 'location=yes');
//            //top.finestra.addEventListener('exit', function (event) { top.finestra = null; });
//        } else {
//            getLoading("off");
//            myApp.addNotification({
//                title: 'ATTENZIONE!',
//                message: "Connessione assente",
//                hold: 4000
//            });
//        }

//    }


    function getContents(obj, url, id) {
         myApp.hideIndicator();

        //alert(obj, url);
        //consolelog(id);
        //alert(checkConnection());
        if (checkConnection()) {
  
            $.ajax({
                type: 'post',
                url: "http://orphanews.gictest.com/parser.ashx?obj=" + obj + "&link=" + url,
                dataType: "html",
                beforeSend : function(){
                    //getLoading("on");
                    myApp.showIndicator();
                },
                error: function(){
                    //getLoading("off");
                    myApp.hideIndicator();
                    myApp.addNotification({
                        title: 'ATTENZIONE!',
                        message: "Connessione assente",
                        hold: 3000
                    });
                },
                success: function (data) {
                    //alert(data);
                   creaOggetto("data"+obj+escape(url)+id,data,9999);
                   getContentsInPage(data,obj, url, id);
                }
          });
        }else{
            // getLoading("off");
            if (ControllaSeEsiste("data"+obj+escape(url)+id)){
                getContentsInPage(leggiOggetto("data"+obj+escape(url)+id),obj, url, id);
            }else{
                myApp.addNotification({
                                  title: 'ATTENZIONE!',
                                  message: "Connessione assente",
                                  hold: 4000
                                  });
            }
        }
}

function getContentsInPage(data,obj, url, id) {
    
                   myApp.hideIndicator();

                    if (obj == "list") {
                        $(".list ul").html("");
                        currYear = "";
                        ContaYear=0;
                        $(data).find('a').each(function (index) {
                            if ($(this).attr("href").indexOf("http://www.orpha.net/actor/ItaliaNews") != -1) {
                                Year = $(this)[0].innerHTML.split(" ");
                                Year = Year[Year.length-1];
                                vettTitles.push($(this)[0].innerHTML);
                                if (currYear != Year) {
                                    $(".list ul").append("<li class='head' onclick='apriTendina(this)'><h2>" + Year + "</h2><i class='fa fa-arrow-circle-down fa-2x'></i></li>");
                                  ContaYear++;
                                }
                                stringToDisplay = "style='display:none'";
                                if (ContaYear==1){
                                      stringToDisplay = "";      
                                }
                                if ( leggiOggetto("lista_preferiti").indexOf(","+index+",") != -1){
                                    $(".list ul").append("<li "+stringToDisplay+" class='item" + index + "'></div><div class='swipeout-content'><i class='fa fa-star fa-2x'></i><h4 onclick='getContents(\"" + $(this)[0].innerHTML + "\",\"" + $(this)[0].href + "\",\"" + index + "\");'>" + $(this)[0].innerHTML + "</h4><a onclick='getContents(\"" + $(this)[0].innerHTML + "\",\"" + $(this)[0].href + "\",\"" + index + "\");'><i class='fa fa-chevron-right fa-2x color-gray'></i></a><i onclick='window.plugins.socialsharing.share(null, null, null,\""+$(this)[0].href+"\")' class='fa fa-share-alt fa-2x'></i></div></li>");
                                    
                                }else{
                                    $(".list ul").append("<li "+stringToDisplay+" class='item" + index + "'></div><div class='swipeout-content'><i onclick='aggiungiPreferiti(this," + index + ")' class='fa fa-star-o fa-2x'></i><h4 onclick='getContents(\"" + $(this)[0].innerHTML + "\",\"" + $(this)[0].href + "\",\"" + index + "\");'>" + $(this)[0].innerHTML + "</h4><a onclick='getContents(\"" + $(this)[0].innerHTML + "\",\"" + $(this)[0].href + "\",\"" + index + "\");'><i class='fa fa-chevron-right fa-2x color-gray'></i></a><i onclick='window.plugins.socialsharing.share(null, null, null,\""+$(this)[0].href+"\")' class='fa fa-share-alt fa-2x'></i></div></li>");
                                }
                                currYear = Year;                            
                                
                            }
                        });
                        dettaglio = false;
                     }else{
                        
                        //////////////////////////////////////////////  crea dettaglio   ////////////////////////////
                       
                       

                         $(".view.dettaglio .content-block").html("");
                            $(data).find('a.RubriqueMenu').each(function (index) {
                                anchor = $(this)[0].href.split("#");
                                vettAnchors.push(anchor[1]);
                                vettTitlesAnchors.push($(this)[0].innerHTML);
                                consolelog("contenuto: " + $("#menu .content-block ul").html());
                                $("#menu .content-block ul").append("<li onclick='vaiPagina(" + index + ")'><a href=\"#\">" + $(this)[0].innerHTML + "</a></li>");   
                            });
                            
                            $(data).find('table').each(function (index) {
                               
                                if ($(this)[0].width == "590") {
                                    testo = strip_tags($(this)[0].innerHTML,"<a><img><span><video><object><strong><br></br>");
                                    lastRow = 0;
                                    
                                    
                                    //consolelog(vettAnchors.length);
                                    for (i=0;i<vettAnchors.length;i++){
                                            search = '<a name="'+vettAnchors[i]+'"></a>';
                                            currentRow =    testo.indexOf(search);
                                            currentPage = testo.substring(lastRow, currentRow).trim();
                                            if (currentPage != ""){
                                                vettPages.push({
                                                     html: testo.substring(lastRow, currentRow),   
                                                 });
                                                //consolelog(search);
                                                //consolelog(lastRow);
                                                //consolelog(currentRow);
                                                //consolelog(vettPages[i]);
                                                
                                                $('.swiper-wrapper.principale').append("<div id='pagina" + i + "' class='swiper-slide pagina'>"+
                                                        "<div class='swiper-container swiper-container-v'>"+
                                                          "<div class='swiper-wrapper'>"+
                                                              "<div class='swiper-slide'>"+
                                                                  "<p onclick='window.plugins.socialsharing.share(null, null, null,\""+url+"#"+vettAnchors[i-1]+"\")' class='shareParagrafo'>"+
                                                                    "<span>Condividi questo articolo&emsp;</span><i class='fa fa-share-alt-square fa-2x color-gray'></i>"+
                                                                  "</p>" + testo.substring(lastRow, currentRow) + 
                                                              "</div>"+
                                                              "<div class='swiper-slide'>"+
                                                                  "<p class='finePagina'></p>"+
                                                              "</div>"+                                                                                                                       
                                                          "</div>"+                                                       
                                                        "</div>"+
                                                      "</div>"
                                                  );
                                                
                                            }
                                            lastRow = currentRow+search.length;
                                    }
                                    //alert(vettAnchors.length);
                                    lastPage = "";
                                    $(data).find('.FonctionMenu').each(function () {
                                        lastPage = $(this)[0].innerHTML;
                                    });
                                    if (lastPage != ""){
                                        vettPages.push({
                                             html: lastPage,   
                                         });
                                        $('.swiper-wrapper').append("<div class='swiper-slide'>" + lastPage + "</div>");
                                        //$("#menu .content-block ul").append("<li onclick='vaiPagina(" + vettPages.length + ")'><a href=\"#\">LAST PAGE</a></li>");  
                                    }

                                    $('.titoloNews > p.data').html(vettTitles[id - 1]);
                                    $('.titoloNews > p.paragrafo').html(vettTitlesAnchors[0]);
                                    $('#contColofon').append($('.swiper-slide:last-child').html());
                                    
                                    var parole = document.getElementById("contColofon").childNodes;
                                    $(parole[0]).wrap('<h3></h3>');
                                    $('.swiper-slide:last-child').remove();
                                    $('#menu li:last-child').remove();
                                    
                                    
                                }

                            }); // fine ciclo
                           

                            $(".view.dettaglio").fadeIn(200, function () {

                                //consolelog(vettTitlesAnchors);
                                var mySwiperV = myApp.swiper('.swiper-container-v', {                                       
                                        direction: 'vertical',
                                        slidesPerView: 'auto',
                                        freeMode: true, 
                                        freeModeMomentum: true,
                                        freeModeMomentumRatio:0.7,
                                        freeModeMomentumBounce: true,
                                        loop: false,
                                        freeModeFluid: true,
                                        freeModeMomentumBounceRatio: 15,                                                           
                                });
                                mySwiperH = myApp.swiper('.swiper-container-h', {
                                    speed: 400,
                                    slidesPerView: 1,                                   
                                    mousewheelControl: true,                                   
                                    watchSlidesVisibility: true,
                                    onSlideChangeEnd:
                                        function (swiper) {
                                            $('.titoloNews > p.paragrafo').html(vettTitlesAnchors[mySwiperH.activeIndex]);
                                        },
                                    onDestroy:
                                        function (swiper) {                                            
                                            mySwiperH.removeAllSlides();
                                            vettAnchors = [];
                                            vettTitlesAnchors = [];
                                            $("#menu .content-block ul, #contColofon").html("");
                                            consolelog("distrutto");

                                        }

                                });
                                
                                //mySwiper.update(true);

                                dettaglio = true;
                                //consolelog(vettPages);
                            });


                           
                            
                    }
       
    }

    function chiudiDettaglio() {       
        mySwiperH.removeAllSlides();
        mySwiperH.destroy(true, true);
        $('.dettaglio').fadeOut(200);
    }
    
    //if (localStorage) {

    //    if (localStorage.addPref) {
    //        $('#mieNews .miaList ul').append(localStorage.addPref);
           
    //    }
    //}
    function aggiungiPreferiti(obj, index) {
        //alert(obj);
        //alert(index);
        creaOggetto("lista_preferiti",leggiOggetto("lista_preferiti")+","+index+",",9999);
        creaOggetto("preferiti",parseInt(leggiOggetto("preferiti"))+1,9999);
        //preferiti++;
        var copia = $(obj).closest('li').clone();
        var miaNews = copia.addClass('swipeout').append("<div class='swipeout-actions-right'><a class='swipeout-delete' href='#' onclick='eliminaPreferiti(" + index + ")'>elimina</a><a class='swipeout-close swipeout-overswipe' href='#'>annulla</a></div>");
        $('#mieNews .miaList ul').append(miaNews);
        $(obj).attr('class', 'fa fa-star fa-2x').attr('onclick','');       
        $('.badge').html(leggiOggetto("preferiti"));

        creaOggetto("preferite",$('#mieNews .miaList ul').html(),9999);
        //preferite.push('<li class="swipeout">'+miaNews.html()+'</li>');
        //creaOggetto("preferite",leggiOggetto("preferite")+'<li class="swipeout">'+miaNews.html()+'</li>',9999);
        
        //localStorage.addPref = preferite;
        //alert(preferite.length);
        
    }

    function eliminaPreferiti(index) {
        
        creaOggetto("lista_preferiti",leggiOggetto("lista_preferiti").replace(","+index+",",""),9999);
        
        //alert(leggiOggetto("preferiti"));
        creaOggetto("preferiti",leggiOggetto("preferiti")-1,9999);
        //alert(leggiOggetto("preferiti"));
        $('.list li.item' + index).find('.fa-star').attr('class', 'fa fa-star-o fa-2x').attr('onclick', 'aggiungiPreferiti(this,'+ index +')');
        setTimeout(function(){
            creaOggetto("preferite",$('#mieNews .miaList ul').html(),9999); 
        },1000);
        
        $('.badge').html(leggiOggetto("preferiti"));

    };

    function getLoading(tipo) {
        if (tipo != "on") {
            $(".loading").fadeOut(150);
        } else {
            $(".loading").fadeIn(150);
        }
    }

    function vaiPagina(id){
       // var pagina = $(obj).index();
        myApp.closePanel('left');
        setTimeout(function () { 
            mySwiperH.slideTo(id, 800, true);
        }, 300);

    }
    
    
    
 
function strip_tags(input, allowed) {
  //  discuss at: http://phpjs.org/functions/strip_tags/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Luke Godfrey
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Pul
  //    input by: Alex
  //    input by: Marc Palau
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: Bobby Drake
  //    input by: Evertjan Garretsen
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Eric Nagel
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Tomasz Wesolowski
  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
  //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
  //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
  //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
  //   returns 2: '<p>Kevin van Zonneveld</p>'
  //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
  //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
  //   example 4: strip_tags('1 < 5 5 > 1');
  //   returns 4: '1 < 5 5 > 1'
  //   example 5: strip_tags('1 <br/> 1');
  //   returns 5: '1  1'
  //   example 6: strip_tags('1 <br/> 1', '<br>');
  //   returns 6: '1 <br/> 1'
  //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
  //   returns 7: '1 <br/> 1'

  allowed = (((allowed || '') + '')
    .toLowerCase()
    .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

function initPushwoosh()
{

    var pushNotification = cordova.require("com.pushwoosh.plugins.pushwoosh.PushNotification");

    if (device.platform.toLowerCase() == 'ios'){
        // Do Something
    } else if(device.platform.toLowerCase() == 'android'){  
        //set push notifications handler
        document.addEventListener('push-notification', function(event) {
            var title = event.notification.title;
            var userData = event.notification.userdata;
                                     
            if(typeof(userData) != "undefined") {
               // console.log('user data: ' + JSON.stringify(userData));
            }
                                         
            //alert(title);
        });
     
        //initialize Pushwoosh with projectid: "990163811242", pw_appid : "A867E-10DC1". This will trigger all pending push notifications on start.
        pushNotification.onDeviceReady({ projectid: 759807681726, pw_appid : "A867E-10DC1" });
     
        //register for pushes
        pushNotification.registerDevice(
            function(status) {
                var pushToken = status;
                //console.log('push token: ' + pushToken);
            },
            function(status) {
               // console.log(JSON.stringify(['failed to register ', status]));
            }
        );
    } else {
        // Do something for desktop
        //set push notifications handler
        document.addEventListener('push-notification', function(event) {
                    //get the notification payload
                    var notification = event.notification;
     
                    //display alert to the user for example
                    //console.warn(JSON.stringify(notification));
        });
     
        //initialize the plugin
        pushNotification.onDeviceReady({ appid: "A867E-10DC1", serviceName: "" });
     
        //register for pushes
        pushNotification.registerDevice(
            function(status) {
                var pushToken = status;
                console.warn('push token: ' + pushToken);
            },
            function(status) {
                console.warn(JSON.stringify(['failed to register ', status]));
            }
        );
    }
}
